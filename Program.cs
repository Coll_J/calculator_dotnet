﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    class Program
    {
        char[] operators = {'(', ')', '-', '+', '/', '*', '^'};

        bool checkHierarchy(char top, char current)
        {
            if(top == current)
                return false;
            if(top == '^')
                return true;
            if(Array.IndexOf(operators, top) - Array.IndexOf(operators, current) > 1)
                return true;
            return false;
        }
        List<KeyValuePair<bool, double>> toPostfix(string infix)
        {
            List<KeyValuePair<bool, double>> result = new List<KeyValuePair<bool, double>>();
            Stack<char> op = new Stack<char>();

            bool topopd = false;

            foreach(char c in infix)
            {
                if(operators.Contains(c))
                {
                    topopd = false;

                    switch(c)
                    {
                        case '(':
                        op.Push(c);
                        break;

                        case ')':
                        while(op.Count > 0 && op.Peek() != '(')
                        {
                            char temp = op.Pop();
                            result.Add(new KeyValuePair<bool, double>(true, (double)temp));
                        }
                        if(op.Count > 0)
                            op.Pop();
                        break;

                        default:
                        if(op.Count == 0)
                            op.Push(c);
                        else if(checkHierarchy(op.Peek(), c))
                        {
                            while(op.Count > 0 && op.Peek() != '(')
                            {
                                char temp = op.Pop();
                                result.Add(new KeyValuePair<bool, double>(true, (double)temp));
                            }
                            op.Push(c);   
                        }
                        else
                            op.Push(c);
                        break;
                    }

                }
                else
                {
                    if(Char.IsLetter(c))
                    {
                        Console.WriteLine("Non-numeric character! Try Again.\n\n");
                        result.Clear();
                        return result;
                    }
                    if(topopd)
                    {
                        double temp = result.ElementAt(result.Count - 1).Value;
                        result.RemoveAt(result.Count - 1);
                        temp = temp * 10 + (double)Char.GetNumericValue(c);
                        result.Add(new KeyValuePair<bool, double>(false, temp));
                    }
                    else
                        result.Add(new KeyValuePair<bool, double>(false, (double)Char.GetNumericValue(c)));
                    topopd = true;
                }
            }

            while(op.Count>0)
            {
                char temp = op.Pop();
                result.Add(new KeyValuePair<bool, double>(true, (double)temp));
            }
            return result;
        }

        double calculatePostfix(List<KeyValuePair<bool, double>> exp)
        {
            Stack<double> result = new Stack<double>();

            foreach(KeyValuePair<bool, double> val in exp)
            {
                if(!(val.Key))
                {
                    result.Push(val.Value);
                }
                else
                {
                    double numb2 = result.Pop();
                    double numb1 = result.Pop();
                    switch(val.Value)
                    {
                        case '+':
                        result.Push(numb1+numb2);
                        break;

                        case '-':
                        result.Push(numb1-numb2);
                        break;

                        case '*':
                        result.Push(numb1*numb2);
                        break;

                        case '/':
                        result.Push(numb1/numb2);
                        break;

                        case '^':
                        result.Push(Math.Pow(numb1, numb2));
                        break;
                        default:
                        break;
                    }
                }
            }

            return result.Pop();
        }

        Program()
        {
            Console.Write("This calculator implement infix to postfix conversion\n" + 
                            "Supports Addition(+), Substraction(-), Multiplication(*), Division(/), and Exponent(^)\n" +
                            "User can input full mathematical expression in one line without space\n" +
                            "\nExample: 1+2*(3+4)-5\n\n");
        }
        static void Main(string[] args)
        {
            string trystr = "";

            Program prog = new Program();
            List<KeyValuePair<bool, double>> ttt;
            double result;

            while(true)
            {
                Console.WriteLine("! Type 'quit' to exit program !\n" + "input:");
                trystr = Console.ReadLine();
                if(trystr.Equals("quit")) break;
                ttt = prog.toPostfix(trystr);
                if(ttt.Count == 0) continue;
                result = prog.calculatePostfix(ttt);

                Console.WriteLine("Result: " + result + "\n");
            }
        }
    }
}
